﻿using CoreMongo.Web.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMongo.Web.ViewComponents
{
    public class CategoriaViewComponent : ViewComponent
    {
        private readonly ICategoriaService _categoriaService;
        public CategoriaViewComponent(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;
        }

        public IViewComponentResult Invoke(string selectedID)
        {
            return View(_categoriaService.List().Select(x => new SelectListItem(x.Descricao, x.ID, x.ID ==selectedID)));
        }
    }
}
