﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Models.Base;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace CoreMongo.Web.Persistence.Contexts
{
    public class AppDbContext
    {
        //public IMongoCollection<Categoria> Categorias { get; set; }
        public IMongoDatabase Database { get; private set; }
        public IMongoCollection<Categoria> Categorias { get; private set; }
        public IMongoCollection<Despesa> Despesas { get; private set; }

        public AppDbContext()
        {
            MapClass();

            var client = new MongoClient("mongodb://localhost:27017");
            Database = client.GetDatabase("ControleFinanceiro");
            Categorias = Database.GetCollection<Categoria>("Categorias");
            Despesas = Database.GetCollection<Despesa>("Despesas");
        }

        private void MapClass()
        {
            BsonClassMap.RegisterClassMap<BaseModel>(cm =>
            {
                //cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.SetIsRootClass(true);
                cm.MapIdProperty(c => c.ID).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            BsonClassMap.RegisterClassMap<Categoria>(cm =>
            {
                cm.SetIgnoreExtraElements(true);
                cm.SetDiscriminator("Categorias");
                cm.MapMember(c => c.Descricao);
                //cm.MapMember(c => c.Cor);
                cm.MapMember(c => c.Ativo);
            });

            BsonClassMap.RegisterClassMap<Despesa>(cm =>
            {
                cm.SetIgnoreExtraElements(true);
                cm.SetDiscriminator("Despesas");
                cm.MapMember(c => c.Descricao);
                cm.MapMember(c => c.Data).SetSerializer(new DateTimeSerializer(dateOnly: true));
                cm.MapMember(c => c.Valor);
                //cm.MapMember(c => c.QuantidadeParcelas);
                cm.MapMember(c => c.CategoriaID);
                cm.MapMember(c => c.Pago);
                cm.MapMember(c => c.DespesaSuperiorID);
            });
        }
    }
}