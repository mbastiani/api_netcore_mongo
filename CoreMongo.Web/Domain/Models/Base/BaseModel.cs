﻿namespace CoreMongo.Web.Domain.Models.Base
{
    public abstract class BaseModel
    {
        public string ID { get; set; }
    }
}
