﻿using CoreMongo.Web.Domain.Models.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace CoreMongo.Web.Domain.Models
{
    public class Categoria: BaseModel
    {
        public Categoria()
        {
            Ativo = true;
        }

        [Display(Name ="Descrição")]
        public string Descricao { get; set; }

        [UIHint("_SimNaoTemplate")]
        public bool Ativo { get; set; }
    }
}
