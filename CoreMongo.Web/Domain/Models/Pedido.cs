﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMongo.Web.Domain.Models
{
    public class Pedido
    {
        public Pedido()
        {
            Itens = new List<Item>();
        }

        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public List<Item> Itens { get; set; }
    }
}