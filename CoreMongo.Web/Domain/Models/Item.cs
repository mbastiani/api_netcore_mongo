﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMongo.Web.Domain.Models
{
    public class Item
    {
        public long ProdutoID { get; set; }
        public decimal Valor { get; set; }
        public string Observacao { get; set; }
    }
}
