﻿using CoreMongo.Web.Domain.Models.Base;

namespace CoreMongo.Web.Domain.Models
{
    public class Usuario: BaseModel
    {
        public string Login { get; set; }
        public string Senha { get; set; }
    }
}
