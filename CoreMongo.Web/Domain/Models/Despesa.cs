﻿using CoreMongo.Web.Domain.Models.Base;
using System;

namespace CoreMongo.Web.Domain.Models
{
    public class Despesa: BaseModel
    {
        public Despesa()
        {
            Data = DateTime.Today;
            Pago = false;
        }

        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public decimal Valor { get; set; }
        public int QuantidadeParcelas { get; set; }
        public bool Pago { get; set; }

        public string CategoriaID { get; set; }
        public string DespesaSuperiorID { get; set; }
    }
}
