﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services.Base;

namespace CoreMongo.Web.Domain.Services
{
    public interface ICategoriaService : IBaseService<Categoria>
    {
        long CountPorDescricao(string descricao, string id);
    }
}
