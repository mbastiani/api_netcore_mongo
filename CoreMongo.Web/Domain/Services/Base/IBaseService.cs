﻿using System.Collections.Generic;

namespace CoreMongo.Web.Domain.Services.Base
{
    public interface IBaseService<TModel>
    {
        IEnumerable<TModel> List();
        string Add(TModel obj);
        TModel FindById(string id);
        void Update(TModel obj);
        void Remove(string id);
    }
}
