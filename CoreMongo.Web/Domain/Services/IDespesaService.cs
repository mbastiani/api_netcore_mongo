﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services.Base;

namespace CoreMongo.Web.Domain.Services
{
    public interface IDespesaService : IBaseService<Despesa>
    {
    }
}
