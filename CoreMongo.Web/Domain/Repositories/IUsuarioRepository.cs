﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Repositories.Base;

namespace CoreMongo.Web.Domain.Repositories
{
    public interface IUsuarioRepository: IBaseRepository<Usuario>
    {
    }
}
