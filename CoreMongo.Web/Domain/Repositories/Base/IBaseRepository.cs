﻿using System.Collections.Generic;

namespace CoreMongo.Web.Domain.Repositories.Base
{
    public interface IBaseRepository<TModel>
    {
        IEnumerable<TModel> List();
        TModel Add(TModel obj);
        TModel FindById(string id);
        void Update(TModel obj);
        void Remove(string id);
    }
}
