﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreMongo.Web.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreMongo.Web.Controllers
{
    public class PedidoController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new Pedido());
        }

        [HttpPost]
        public IActionResult Create(Pedido pedido)
        {
            return View(pedido);
        }
    }
}