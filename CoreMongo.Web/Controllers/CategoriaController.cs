﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services;

namespace CoreMongo.Web.Controllers
{
    public class CategoriaController : Controller
    {
        private readonly ICategoriaService _categoriaService;

        public CategoriaController(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;
        }

        public IActionResult Index()
        {
            return View(_categoriaService.List());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new Categoria());
        }

        [HttpPost]
        public IActionResult Create(Categoria obj)
        {
            if (!ModelState.IsValid)
            {
                return View(obj);
            }

            _categoriaService.Add(obj);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            var obj = _categoriaService.FindById(id);

            if(obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        [HttpPost]
        public IActionResult Edit(Categoria obj)
        {
            if (!ModelState.IsValid)
            {
                return View(obj);
            }

            _categoriaService.Update(obj);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            var obj = _categoriaService.FindById(id);

            if (obj == null)
            {
                return NotFound();
            }

            _categoriaService.Remove(id);

            return RedirectToAction("Index");
        }
    }
}