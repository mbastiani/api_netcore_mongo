﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreMongo.Web.Controllers
{
    public class DespesaController : Controller
    {
        private readonly IDespesaService _despesaService;
        private readonly ICategoriaService _categoriaService;

        public DespesaController(IDespesaService despesaService, ICategoriaService categoriaService)
        {
            _despesaService = despesaService;
            _categoriaService = categoriaService;
        }

        public IActionResult Index()
        {
            return View(_despesaService.List());
        }

        [HttpGet]
        public IActionResult Create()
        {
            //PopulateDropDowns();
            return View(new Despesa());
        }

        [HttpPost]
        public IActionResult Create(Despesa obj)
        {
            if (!ModelState.IsValid)
            {
                //PopulateDropDowns();
                return View(obj);
            }

            _despesaService.Add(obj);

            //return RedirectToAction("Index");
            return View(obj);
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            var obj = _despesaService.FindById(id);

            if(obj == null)
            {
                return NotFound();
            }

            return View(obj);
        }

        [HttpPost]
        public IActionResult Edit(Despesa obj)
        {
            if (!ModelState.IsValid)
            {
                return View(obj);
            }

            _despesaService.Update(obj);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            var obj = _despesaService.FindById(id);

            if (obj == null)
            {
                return NotFound();
            }

            _despesaService.Remove(id);

            return RedirectToAction("Index");
        }

        private void PopulateDropDowns()
        {
            ViewBag.Categorias = _categoriaService.List().Select(x=> new SelectListItem(x.Descricao, x.ID));
        }
    }
}