﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services;
using FluentValidation;
using System;

namespace CoreMongo.Web.Services.Validators
{
    public class CategoriaValidator : AbstractValidator<Categoria>
    {
        private ICategoriaService _categoriaService;

        public CategoriaValidator(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;

            RuleFor(d => d)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Objeto Categoria não encontrato");
                });

            RuleFor(d => d.Descricao)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("É necessário informar uma descrição")
                .Must(ValidarDescricaoExistente).WithMessage("Já existe uma categoria com essa Descrição");

            RuleFor(d => d.Ativo)
                .NotNull().WithMessage("É necessário informar se a categoria está ativa ou inativa");
        }

        private bool ValidarDescricaoExistente(Categoria obj, string descricao)
        {
            if (_categoriaService.CountPorDescricao(descricao, obj.ID) > 0)
            {
                return false;
            }

            return true;
        }
    }
}