﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services;
using FluentValidation;
using System;

namespace CoreMongo.Web.Services.Validators
{
    public class DespesaValidator : AbstractValidator<Despesa>
    {
        private ICategoriaService _categoriaService;

        public DespesaValidator(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;

            RuleFor(d => d)
                .NotNull()
                .OnAnyFailure(x =>
                {
                    throw new ArgumentNullException("Objeto Despesa não encontrato");
                });

            RuleFor(d => d.Descricao)
                .NotEmpty().WithMessage("É necessário informar uma descrição");

            RuleFor(d => d.CategoriaID)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("É necessário informar uma categoria")
                .Must(ValidarCategoria).WithMessage("Categoria inexistente");

            RuleFor(d => d.Data)
                .NotEmpty().WithMessage("É necessário informar uma data");

            RuleFor(d => d.QuantidadeParcelas)
                .NotEmpty().WithMessage("É necessário informar a quantidade de parcelas");
        }

        private bool ValidarCategoria(string categoriaID)
        {
            if (_categoriaService.FindById(categoriaID) == null)
            {
                return false;
            }

            return true;
        }
    }
}
