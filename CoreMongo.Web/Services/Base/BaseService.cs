﻿using CoreMongo.Web.Domain.Models.Base;
using CoreMongo.Web.Persistence.Contexts;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Collections.Generic;

namespace CoreMongo.Web.Services.Base
{
    public class BaseService<TModel> where TModel: BaseModel
    {
        protected readonly AppDbContext _context;
        protected readonly IMongoCollection<TModel> _collection;
        protected readonly FindOptions _findOptions = new FindOptions { Collation = new Collation("pt", strength: CollationStrength.Primary) };
        public BaseService(AppDbContext context)
        {
            _context = context;
            _collection = _context.Database.GetCollection<TModel>(BsonClassMap.LookupClassMap(typeof(TModel)).Discriminator);
        }

        public virtual string Add(TModel obj)
        {
            _collection.InsertOne(obj);
            return obj.ID;
        }

        public virtual void AddMany(List<TModel> objs)
        {
            _collection.InsertMany(objs);
        }

        public virtual void Update(TModel obj)
        {
            _collection.ReplaceOne(x => x.ID == obj.ID, obj);
        }

        public virtual TModel FindById(string id)
        {
            return _collection.Find<TModel>( x=> x.ID == id).FirstOrDefault();
        }

        public virtual IEnumerable<TModel> List()
        {
            return _collection.Find(x => true).ToList();
        }

        public virtual void Remove(string id)
        {
            _collection.DeleteOne(x => x.ID == id);
        }
    }
}
