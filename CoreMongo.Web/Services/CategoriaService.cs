﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Services;
using CoreMongo.Web.Persistence.Contexts;
using CoreMongo.Web.Services.Base;
using MongoDB.Driver;

namespace CoreMongo.Web.Services
{
    public class CategoriaService : BaseService<Categoria>, ICategoriaService
    {
        public CategoriaService(AppDbContext context) : base(context)
        {
        }

        public long CountPorDescricao(string descricao, string id)
        {
            return _context.Categorias.Find(x => x.Descricao == descricao && (x.ID != id || string.IsNullOrWhiteSpace(id)),
                _findOptions).CountDocuments();
        }
    }
}
