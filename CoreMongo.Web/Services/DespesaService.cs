﻿using CoreMongo.Web.Domain.Models;
using CoreMongo.Web.Domain.Repositories;
using CoreMongo.Web.Domain.Services;
using CoreMongo.Web.Persistence.Contexts;
using CoreMongo.Web.Services.Base;
using CoreMongo.Web.Services.Validators;
using FluentValidation;
using System;
using System.Collections.Generic;
using CoreMongo.Web.Util;

namespace CoreMongo.Web.Services
{
    public class DespesaService : BaseService<Despesa>, IDespesaService
    {
        public DespesaService(AppDbContext context) : base(context)
        {
        }

        public override string Add(Despesa obj)
        {
            if (obj.QuantidadeParcelas == 1)
            {
                return base.Add(obj);
            }

            var listaDespesas = new List<Despesa>();

            for (int i = 2; i <= obj.QuantidadeParcelas; i++)
            {
                var newObj = obj.Clone();
                newObj.Descricao = newObj.Descricao + $" {i}/{obj.QuantidadeParcelas}";
                listaDespesas.Add(newObj);
            }

            obj.Descricao = obj.Descricao + $" 1/{obj.QuantidadeParcelas}";

            var id = base.Add(obj);
            listaDespesas.ForEach(x => x.DespesaSuperiorID = id);
            base.AddMany(listaDespesas);

            return string.Empty;
        }

        //public override Despesa FindById(string id)
        //{
        //    var obj = base.FindById(id);

        //    if(obj != null)
        //    {
        //        obj.Categoria = _categoriaRepository.FindById(obj.CategoriaID);
        //    }

        //    return obj;
        //}
    }
}
